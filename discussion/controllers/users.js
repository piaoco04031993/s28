// [SECTION] Dependencies and Modules
const User = require('../models/User');
const bcrypt = require('bcrypt');

// [SECTION] User Functionalities

	// Create a new user.
	module.exports.registerUser = (reqBody) => {

		let fName = reqBody.firstName;
		let lName = reqBody.lastName;
		let email = reqBody.email;
		let passW = reqBody.password;

		let newUser = new User ({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(passW, 10) 
		});


		return newUser.save().then((user, error) => {
			if (user) {
				return user;
			} else 	{
				return 'Failed to Create User';
			}
		});
	};

	// Retrieve all users
	module.exports.getallUsers = () => {
		return User.find({}).then(resultOfQuery => {
			// display the result of the query
			return resultOfQuery;
			});
	}

	// Retrieve a single user
	module.exports.getProfile = (data) => {

		return User.findById(data).then(result => {
			result.password = ''; 
			return result;
		})
	}

	// Delete a user
	module.exports.deleteUser = (userId) => {
		return User.findByIdAndRemove(userId).then((removedUser, err) => {
			
			if (removedUser) {
				return `${removedUser}
						Account Deleted Sucessfully`; 
			} else {
				return 'No accounts were Removed!';
			}

		})
	}
	// Update User Details
	module.exports.updateUser = (userId, newContent) => {
			let fName = newContent.firstName;
			let lName = newContent.lastName;
	 		return User.findById(userId).then((foundUser, error) => {
 			if (foundUser) {
 				foundUser.firstName = fName;
 				foundUser.lastName = lName;			
 				return foundUser.save().then((updatedUser, saveErr) => {
 					if (saveErr) {
 						return false;
 					} else {
 						return updatedUser;
 					}

 				})
 			} else {
 				return 'No User Found';
 			}
 		});
 }
