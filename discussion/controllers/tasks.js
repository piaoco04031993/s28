// [SECTION] Dependencies and Modules
const Task = require('../models/Task');

// [SECTION] functionalities

	// Create New Task 
	module.exports.createTask = (clientInput) => {
		let taskName = clientInput.name
		let newTask = new Task({
			name: taskName
		});
		return newTask.save().then((task, error) => {
			if (error) {
				return 'Saving New Task Failed';
			} else {
				return 'A New Task Created';
			}
		})
	}

	// Retrieve All tasks
	module.exports.getAllTasks = () => {
		return Task.find({}).then(searchResult => {
			return searchResult;
		})
	}

	// Retrieve Single Task
	module.exports.getTask = (data) => {
		return Task.findById(data).then(searchResult => {
			return searchResult;
		})
	}

	// Update Task
	module.exports.updateTask = (taskId) => {
		return Task.findById(taskId).then(task => {
		if (task) {
			
 				task.status = 'complete'
 				return task.save().then((updatedTask, saveErr) => {
 					if (saveErr) {
 						return false;
 					} else {
 						return updatedTask;
 					}

 				})
 			} else {
 				return 'No Task Found';
 			}
		})
	}

	// Delete a Task
	module.exports.deleteTask = (taskId) => {
		return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
			
			if (removedTask) {
				return `${removedTask}
						Task Deleted Sucessfully`; 
			} else {
				return 'No Task were Removed!';
			}

		});
	}
		
			
