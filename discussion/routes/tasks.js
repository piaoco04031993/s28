// [SECTION] Dependencies and Modules
	const express = require('express');
	const controller = require('../controllers/tasks');

// [SECTION] Routing Component
	const route = express.Router();

// [SECTION] Task Routes
	// Create Task
	route.post('/create', (req, res) => {
		// execute the createTask() from the controller.
		let taskInfo = req.body
		controller.createTask(taskInfo).then(result =>
			res.send(result)
		)
	});

	// Retrieve all tasks
	route.get('/', (req, res) => {
		controller.getAllTasks().then(result => {
			res.send(result);
		})
	}); 

	// Retrieve Single Task
	route.get('/:id', (req, res) => {
		let taskId = req.params.id;
		controller.getTask(taskId).then(result => {
			res.send(result);
		})
	}); 

	// Update Task
	route.put('/:id', (req, res) => {
		let taskId = req.params.id;
		controller.updateTask(taskId).then(result => {
			res.send(result);
		})
	}); 

	// Detele Task
	route.delete('/:id', (req, res) => {
		let taskId = req.params.id;
		controller.deleteTask(taskId).then(result => {
			res.send(result);
		})
	}); 


// [SECTION} Expose Route System
	module.exports = route;